﻿using System;

namespace AspMfg
{
	public class Trip
	{
		public long Id { get; set; }
		public String Name { get; set; }

		public Trip ()
		{
		}
		public Trip (long Id, String Name)
		{
			this.Id = Id;
			this.Name = Name;
		}

		public override String ToString()
		{
			return Name + " (" + Id + ")";
		}
	}
}

